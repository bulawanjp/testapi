`use strict`;

const express = require('express');
const app = express();
const exec = require('child_process').execFile;
const port = 8080;

app.get('/', (req, res) => {
	console.log('GET /');
	res.status(200).json({
		message: 'NEXT BUILD TRY RESTART wewewewewe'
	});
});

app.get('/sayhello/:name', (req, res) => {
	console.log('GET /sayhello : Saying hello to '+ req.params.name);
	res.status(200).json({
		message: `Hello ${req.params.name}!`
	});
});

app.get('/x', async (req, res) => {
	await exec(`${process.cwd()}/restart.sh`, (err, stdout, stderr) => {
		console.log(stdout);
		res.status(200).json({
			console: stdout
		});
	});
});

app.listen(port, (err, result) => {
	if (err)
		throw err
	console.log(`Sample app running on port ${port}`);
});